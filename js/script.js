var bgClass = ['colour1', 'colour2', 'colour3', 'colour4', 'colour5', 'colour6'];


$(document).ready(function(){

	$('.outer_container').fadeIn(500)

	  var slickPlayer = $('.inner_container').slick({
	    infinite: true,
	    autoplay: true,
	    autoplaySpeed: 12000,
	    arrows: false,
	    draggable: false,
	    fade: true,
	    speed: 500,
	    touchMove: false,
	    pauseOnHover: false
	  });

	  var el = document.getElementById('body');
	var border = document.getElementById('copy');

	$('.inner_container').on('beforeChange', function(event, slick, currentSlide, nextSlide){
  		console.log(nextSlide);
  		if(currentSlide === 5)
  			$('.inner_container').slick("slickSetOption", "autoplaySpeed", 12000, 12000);
  		if(currentSlide === 0)
  			$('.inner_container').slick("slickSetOption", "autoplaySpeed", 5000, 5000);
		if (el.classList){
		  $("body").removeClass("colour1 colour2 colour3 colour4 colour5 colour6")
		  el.classList.add(bgClass[nextSlide]);
		}else{
		  $("body").removeClass("colour1 colour2 colour3 colour4 colour5 colour6")
		  el.className += ' ' + bgClass[nextSlide];
	  	}
	});
});


document.onload = function(){

}